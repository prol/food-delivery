﻿using FoodDelivery.App.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Deliveryapp.Infra.DataAccess.Contexts
{
    public class FoodDeliveryLocalContext : DbContext
    {
        private readonly string dbConnectionString;

        public DbSet<Product> Products { get; set; }

        public FoodDeliveryLocalContext()
        {
            
        }

        public FoodDeliveryLocalContext(string dbConnectionString)
        {
            this.dbConnectionString = dbConnectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            // optionsBuilder.UseSqlServer(dbConnectionString);
            optionsBuilder.UseSqlite(dbConnectionString);
        }

    }
}
