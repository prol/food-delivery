﻿using Deliveryapp.Infra.DataAccess.Contexts;
using FoodDelivery.App.Domain.Entities;
using FoodDelivery.App.Domain.Interfaces.Repositories;
using FoodDelivery.Common.Infra.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Deliveryapp.Infra.DataAccess.Repositories.Products
{
    public class SQLiteProductsRepository : EntityFrameworkRepositoryBase<Guid, Product>, IProductRepository
    {
        public SQLiteProductsRepository(string devicePlatform)
        {
            string dbPath = "Filename=";
            const string dbFileName = "foodedelivery.sqlite";

            switch (devicePlatform)
            {
                case "UWP":
                    dbPath += Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), dbFileName);
                    break;
                case "iOS":
                    dbPath += Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", "data", dbFileName);
                    break;
                case "Android":
                    dbPath += Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), dbFileName);
                    break;
            }

            db = new FoodDeliveryLocalContext(dbPath);
        }
    }
}
