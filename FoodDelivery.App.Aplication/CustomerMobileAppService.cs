﻿using Deliveryapp.Infra.DataAccess.Repositories.Orders;
using FoodDelivery.App.Aplication.Models.Dtos;
using FoodDelivery.App.Domain.Entities;
using FoodDelivery.Common.Infra.Helper.Serializer;
using FoodDelivery.Domain.Entities;
using FoodDelivery.Domain.Services;
using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.App.Aplication
{
    public class CustomerMobileAppService : IAppService
    {
       
        private string token;


        public IEnumerable<Product> GetAllProducts()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var result = client.GetAsync("https://fooddeliveryproductmicroserviceapi.azurewebsites.net/api/products").Result;

            var serializedProducts = result.Content.ReadAsStringAsync().Result;
            var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(serializedProducts);

            return products;
        }

        public async Task BuyProductAsync(Product product)
        {
            var orderItem = new OrderItem
            {
                Id = Guid.NewGuid(),
                Name = product.Name,
                CodigoAloha = product.CodigoAloha,
                ProductId = product.Id,
                Price = product.Price,
                SKU = product.SKU,
                Quantity = 1,
            };
            var orderItems = new List<OrderItem>();
            orderItems.Add(orderItem);

            var order = new Order();
            order.OrderItems = orderItems;

            var orderService = new OrderRemoteService(new OrderMicroserviceRepository(token, new SerializerService()));
            await orderService.CreateOrderAsync(order);
        }


        public bool SignIn(string username, string password)
        {
            token = GetToken(username, password);
            if (String.IsNullOrEmpty(token))
                return false;
            return true;
        }
        public bool SignUp(UserPasswordDto userPassword)
        {
            var token = GetAdminToken();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var serializedUserPassword = JsonConvert.SerializeObject(userPassword);
            var httpContent = new StringContent(serializedUserPassword, Encoding.UTF8, "application/json");
            var result = httpClient.PostAsync("https://fooddelivery-iam-microservice-api-admin.azurewebsites.net/api/UsersAndRoles", httpContent).Result;

            if (!result.IsSuccessStatusCode)
                return false;
            return true;
        }
        private string GetAdminToken()
        {
            var client = new HttpClient();
            var response = client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "https://fooddelivery-iam-microservice-identity.azurewebsites.net/connect/token",

                ClientId = "FoodDeliveryCustomerMobileApp_ClientId",
                //ClientSecret = "secret",
                //Scope = "api1",

                UserName = "admin",
                Password = "DEP@1317#alpha"
            }).Result;

            return response.AccessToken;
        }
        private string GetToken(string username, string password)
        {
            var client = new HttpClient();
            var response = client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "https://fooddelivery-iam-microservice-identity.azurewebsites.net/connect/token",

                ClientId = "FoodDeliveryCustomerMobileApp_ClientId",
                //ClientSecret = "secret",
                //Scope = "api1",

                UserName = username,
                Password = password
            }).Result;

            return response.AccessToken;
        }
    }
}
