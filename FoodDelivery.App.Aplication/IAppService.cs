﻿using FoodDelivery.App.Aplication.Models.Dtos;
using FoodDelivery.App.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.App.Aplication
{
    public interface IAppService
    {
        Task BuyProductAsync(Product product);
        bool SignIn(string username, string password);
        bool SignUp(UserPasswordDto userPassword);
        IEnumerable<Product> GetAllProducts();
    }
}
