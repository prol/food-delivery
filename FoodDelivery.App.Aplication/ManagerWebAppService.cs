﻿using FoodDelivery.App.Aplication.Models.Dtos;
using FoodDelivery.App.Domain.Entities;
using IdentityModel.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace FoodDelivery.App.Aplication
{
    public class ManagerWebAppService
    {
        private string token;
        public IEnumerable<Product> GetAllProducts()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var result = client.GetAsync("https://fooddeliveryproductmicroserviceapi.azurewebsites.net/api/products").Result;

            var serializedProducts = result.Content.ReadAsStringAsync().Result;
            var products = JsonConvert.DeserializeObject<IEnumerable<Product>>(serializedProducts);

            return products;
        }

        public Product GetProduct(Guid productId)
        {
            throw new NotImplementedException();
        }
        public bool AddProduct(Product product)
        {
            throw new NotImplementedException();
        }
        public bool UpdateProduct(Product product)
        {
            throw new NotImplementedException();
        }
        public bool RemoveProduct(Guid productId)
        {
            throw new NotImplementedException();
        }
        public bool SignIn(string username, string password)
        {
            token = GetToken(username, password);
            if (String.IsNullOrEmpty(token))
                return false;
            return true;
        }
        public bool SignUp(UserPasswordDto userPassword)
        {
            var token = GetAdminToken();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
            var serializedUserPassword = JsonConvert.SerializeObject(userPassword);
            var httpContent = new StringContent(serializedUserPassword, Encoding.UTF8, "application/json");
            var result = httpClient.PostAsync("https://fooddelivery-iam-microservice-api-admin.azurewebsites.net/api/UsersAndRoles", httpContent).Result;

            if (!result.IsSuccessStatusCode)
                return false;
            return true;
        }
        private string GetAdminToken()
        {
            var client = new HttpClient();
            var response = client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "https://fooddelivery-iam-microservice-identity.azurewebsites.net/connect/token",

                ClientId = "FoodDeliveryCustomerMobileApp_ClientId",
                //ClientSecret = "secret",
                //Scope = "api1",

                UserName = "admin",
                Password = "DEP@1317#alpha"
            }).Result;

            return response.AccessToken;
        }
        private string GetToken(string username, string password)
        {
            var client = new HttpClient();
            var response = client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "https://fooddelivery-iam-microservice-identity.azurewebsites.net/connect/token",

                ClientId = "FoodDeliveryCustomerMobileApp_ClientId",
                //ClientSecret = "secret",
                //Scope = "api1",

                UserName = username,
                Password = password
            }).Result;

            return response.AccessToken;
        }
    }
}
