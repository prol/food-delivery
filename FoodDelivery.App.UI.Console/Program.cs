﻿using IdentityModel.Client;
using System;
using System.Net.Http;

namespace FoodDelivery.App.UI.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            //https://localhost:54891
            System.Console.WriteLine(GetToken("", ""));
        }

        private static string GetToken(string username, string password)
        {
            var client = new HttpClient();
            var response = client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = "https://fooddelivery-iam-microservice-identity.azurewebsites.net/connect/token",

                ClientId = "PostmanClientId",
                //ClientSecret = "secret",
                //Scope = "api1",

                UserName = username,
                Password = password
            }).Result;

            return response.AccessToken;

        }

    }


}
