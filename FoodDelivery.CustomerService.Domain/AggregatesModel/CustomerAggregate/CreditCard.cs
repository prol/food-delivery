﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.CustomerService.Domain.AggregatesModel.CustomerAggregate
{
    public class CreditCard
    {
        public string creditcardNumber { get; set; }
        public DateTime expirationDate { get; set; }
        public int securityCode { get; set; }
    }
}
