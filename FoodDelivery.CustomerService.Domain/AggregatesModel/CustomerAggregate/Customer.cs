﻿using FoodDelivery.Common.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.CustomerService.Domain.AggregatesModel.CustomerAggregate
{
    public class Customer: EntityBase<Guid>
    {
        public string name { get; set; }

        public string telefone { get; set; }

       

        public Address address { get; set; }

        public CreditCard creditCard { get; set; }
    }
}
