﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.CustomerService.Domain.AggregatesModel.CustomerAggregate
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository repository;

        public CustomerService(ICustomerRepository repository)
        {
            this.repository = repository;
        }


        //User Cases related to customer
        public async Task<bool> AddCustomerAsync(Customer customer)
        {
            customer.Id = Guid.NewGuid();
            await repository.CreateAsync(customer);
            return await repository.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteCustomerAsync(Guid customerId)
        {
            await repository.DeleteAsync(customerId);
            return await repository.SaveChangesAsync() > 0;
        }

        public IReadOnlyCollection<Customer> GetAllCustomers()
        {
            return (IReadOnlyCollection<Customer>)repository.ReadAll();
        }

        public async Task<IReadOnlyCollection<Customer>> GetAllCustomersAsync()
        {
            return (IReadOnlyCollection<Customer>)await repository.ReadAllAsync();
        }

        public async Task<Customer> GetCustomerAsync(Guid customerId)
        {
            return await repository.ReadAsync(customerId);
        }

        public async Task<bool> UpdateCustomerAsync(Customer customer)
        {
            repository.Update(customer);
            return await repository.SaveChangesAsync() > 0;
        }
    }
}
