﻿using FoodDelivery.Common.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.CustomerService.Domain.AggregatesModel.CustomerAggregate
{
    public interface ICustomerRepository: IRepository<Guid, Customer>
    {
    }
}
