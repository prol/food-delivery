﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.CustomerService.Domain.AggregatesModel.CustomerAggregate
{
    public interface ICustomerService
    {
        Task<bool> AddCustomerAsync(Customer customer);
        IReadOnlyCollection<Customer> GetAllCustomers();
        Task<Customer> GetCustomerAsync(Guid customerId);
        Task<IReadOnlyCollection<Customer>> GetAllCustomersAsync();
        Task<bool> UpdateCustomerAsync(Customer customer);
        Task<bool> DeleteCustomerAsync(Guid customerId);
    }
}
