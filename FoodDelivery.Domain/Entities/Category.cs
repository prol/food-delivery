﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace FoodDelivery.App.Domain.Entities
{
    //objeto de valor
    public struct Category : IComparable<Category>
    {
        private string name;
        public string Name { 
            get {
                return name;
            }
            set 
            { 
                if(name.Length <3 || Char.IsLower(value[0]))
                    throw new ArgumentOutOfRangeException("Categoria precisa de pelo menos 3 caracteres e começar com letra maiúscula");
            } }


        public Category(string name)
        {
            if (name.Length < 3 || Char.IsLower(name[0]))
                throw new ArgumentOutOfRangeException("Categoria precisa de pelo menos 3 caracteres e começar com letra maiúscula");
            this.name = name;
        }

        public static Category Parse(string category)
        {
            return new Category(category);
        }

        public static implicit operator String (Category category)
        {
            return category.Name;
        }

        public static implicit operator Category(string category)
        {
            return new Category(category);
        }

        public override bool Equals (object obj)
        {
            return this.ToString() == obj.ToString();
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }

        public int CompareTo(Category other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
