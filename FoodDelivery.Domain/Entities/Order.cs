﻿using FoodDelivery.Common.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Domain.Entities
{
        public class Order : EntityBase<Guid>
        {
            public Guid CustomerId { get; set; }
            public DateTime DateTime { get; set; }
            public IReadOnlyCollection<OrderItem> OrderItems { get; set; }
        }
    }

