﻿using FoodDelivery.Common.Domain.Interfaces.Repositories;
using FoodDelivery.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Domain.Interfaces.Repositories
{
    public interface IOrderRepository: IRepository<Guid, Order>
    {
    }
}
