﻿using FoodDelivery.App.Domain.Entities;
using FoodDelivery.Common.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.App.Domain.Interfaces.Repositories
{
    public interface IProductRepository: IRepository<Guid, Product>
    {
    }
}
