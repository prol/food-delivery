﻿using FoodDelivery.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Domain.Interfaces.Services
{
    public interface IOrderService
    {
        Task CreateOrderAsync(Order order);
    }
}
