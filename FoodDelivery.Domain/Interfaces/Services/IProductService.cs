﻿using FoodDelivery.App.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Domain.Interfaces.Services
{
    public interface IProductService
    {
        IEnumerable<Product> GetAllProducts();
    }
}
