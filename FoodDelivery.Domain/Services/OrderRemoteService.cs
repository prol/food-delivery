﻿using FoodDelivery.Domain.Entities;
using FoodDelivery.Domain.Interfaces.Repositories;
using FoodDelivery.Domain.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Domain.Services
{
    public class OrderRemoteService : IOrderService
    {
        private IOrderRepository orderRepository;

        public OrderRemoteService(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        public async Task CreateOrderAsync(Order order)
        {
           await orderRepository.CreateAsync(order);
        }
    }
}
