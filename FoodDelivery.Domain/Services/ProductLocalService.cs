﻿using FoodDelivery.App.Domain.Entities;
using FoodDelivery.App.Domain.Interfaces.Repositories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.App.Domain.Services
{
    public class ProductLocalService
    {
        private IProductRepository repository;

        public ProductLocalService(IProductRepository repository)
        {
            this.repository = repository;
        }

        //User Cases related to product
        public async Task AddProductAsync(Product product)
        {
            product.Id = Guid.NewGuid();
            await repository.CreateAsync(product);
            await repository.SaveChangesAsync();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return repository.ReadAll();
            
        }

        public async Task UpdateProductAsync (Product product)
        {
            repository.Update(product);
            await repository.SaveChangesAsync();
        }

        public async Task DeleteProductAsync(Guid productId)
        {
            await repository.DeleteAsync(productId);
            await repository.SaveChangesAsync();
        }
    }
}
