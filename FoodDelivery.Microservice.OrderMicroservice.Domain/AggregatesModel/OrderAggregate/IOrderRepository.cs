﻿using FoodDelivery.Common.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate
{
    public interface IOrderRepository : IRepository<Guid, Order>
    {
    }
}
