﻿using FoodDelivery.Common.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate
{
    public class Order: EntityBase<Guid>
    {
        public Guid CustomerId { get; set; }
        public DateTime DateTime { get; set; }
        public IEnumerable<OrderItem> OrderItems { get; set; }

        public Order()
        {
            OrderItems = new List<OrderItem>();
        }
    }
}
