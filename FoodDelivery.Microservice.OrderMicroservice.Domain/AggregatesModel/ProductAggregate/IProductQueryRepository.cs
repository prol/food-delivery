﻿using FoodDelivery.Common.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.ProductAggregate
{
    public interface IProductQueryRepository : IQueryRepository<Guid, Product>
    {
    }
}
