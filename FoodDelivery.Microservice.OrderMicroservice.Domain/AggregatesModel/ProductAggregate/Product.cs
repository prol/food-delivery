﻿using FoodDelivery.Common.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.ProductAggregate
{
    public class Product: EntityBase<Guid>
    {
        public string SKU { get; set; }
        public string Name { get; set; }
        public string CodigoAloha { get; set; }
        public string Descricao { get; set; }
        //public Category category { get; set; }
        public Decimal Price { get; set; }
        public string PhotoUrl { get; set; }
        public int Quantity { get; set; }
    }
}
