using FoodDelivery.Microservices.IamMicroservice.Admin.Api.ExceptionHandling;

namespace FoodDelivery.Microservices.IamMicroservice.Admin.Api.Resources
{
    public interface IApiErrorResources
    {
        ApiError CannotSetId();
    }
}





