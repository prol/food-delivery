using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FoodDelivery.Common.Application.Interfaces.CQRS.Messaging;
using FoodDelivery.Common.Domain.Interfaces.Services;
using FoodDelivery.Common.Infra.Helper.Serializer;
using FoodDelivery.Common.Infra.Messaging.Services;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.ProductAggregate;
using FoodDelivery.Microservices.OrderMicroservice.Application.Services;
using FoodDelivery.Microservices.OrderMicroservice.OrderWorker;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Contexts;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Repositories;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FoodDelivery.Microservices.OrderMicroservice.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            services.AddDbContext<OrderContext>();
            services.AddScoped<IMediatorHandler, AzureServiceBusQueue>();
            services.AddScoped<IProductQueryRepository, ProductMicroserviceQueryRepository>();
            services.AddScoped<DbContext, OrderContext>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IProductQueryService, ProductQueryService>();
            services.AddScoped<ISerializerService, SerializerService>();
            services.AddScoped<IApiApplicationService, ApiApplicationService>();

            services.AddAuthorization();
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = "https://fooddelivery-iam-microservice-identity.azurewebsites.net";
                    options.RequireHttpsMetadata = false;
                    options.ApiName = "OrderMicroservice-ApiResource";

                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
