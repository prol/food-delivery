﻿using FoodDelivery.Common.Application.Interfaces.CQRS.Commands;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands
{
    public abstract class OrderCommand: Command
    {
        public Order Order { get; set; }
    }
}
