﻿using FoodDelivery.Common.Application.Interfaces.CQRS.Events;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Events
{
    public abstract class OrderEvent: Event
    {
        public Order Order { get; set; }
        public bool Success { get; set; }
    }
}
