﻿using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Interfaces
{
    public interface IOrderCommandHandler
    {
        Task<bool> HandleAsync(ProcessOrderCommand processOrderCommand);
    }
}
