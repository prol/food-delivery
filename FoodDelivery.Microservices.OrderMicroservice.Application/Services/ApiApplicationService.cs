﻿using FoodDelivery.Common.Application.Interfaces.CQRS.Messaging;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.Services
{
    public class ApiApplicationService: IApiApplicationService
    {
        private IOrderService orderService;
        private IMediatorHandler bus;

        public ApiApplicationService(IOrderService orderService, IMediatorHandler mediatorHandler)
        {
            this.orderService = orderService;
            this.bus = mediatorHandler;
        }

        public async Task<Order> CreateOrderAsync(Guid customerId, IEnumerable<OrderItem> orderItems)
        {
            var order = orderService.CreateOrder(customerId, orderItems);
            var processOrderCommand = new ProcessOrderCommand(order);
            await bus.EnqueueAsync(processOrderCommand, ProcessOrderCommand.CommandQueueName);

            return order;
        }
    }
}
