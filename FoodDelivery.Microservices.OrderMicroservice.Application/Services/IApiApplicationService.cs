﻿using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.Services
{
    public interface IApiApplicationService
    {
        Task<Order> CreateOrderAsync(Guid customerId, IEnumerable<OrderItem> orderItems);
    }
}
