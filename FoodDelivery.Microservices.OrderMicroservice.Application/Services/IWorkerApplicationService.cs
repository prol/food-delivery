﻿using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.Services
{
    public interface IWorkerApplicationService
    {
        Task ProcessOrderAsync(ProcessOrderCommand processOrderCommand);
    }
}
