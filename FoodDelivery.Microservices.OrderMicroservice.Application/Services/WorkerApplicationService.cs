﻿using FoodDelivery.Common.Application.Interfaces.CQRS.Messaging;
using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands;
using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Events;
using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.Application.Services
{
    public class WorkerApplicationService : IWorkerApplicationService
    {
        private IOrderCommandHandler orderCommandHandler;
        private IMediatorHandler bus;

        public WorkerApplicationService(IOrderCommandHandler orderCommandHandler, IMediatorHandler bus)
        {
            this.orderCommandHandler = orderCommandHandler;
            this.bus = bus;
        }
        public async Task ProcessOrderAsync(ProcessOrderCommand processOrderCommand)
        {
            var commandHandlerSuccess = await orderCommandHandler.HandleAsync(processOrderCommand);
            var orderProcessedEvent = new OrderProcessedEvent(processOrderCommand.Order);
            orderProcessedEvent.Success = true;

            if (!commandHandlerSuccess)
                orderProcessedEvent.Success = false;

            await bus.EnqueueAsync(orderProcessedEvent, OrderProcessedEvent.EventQueueName);
        }
    }
}

