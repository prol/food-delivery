﻿using FoodDelivery.Common.Domain.Interfaces.Services;
using FoodDelivery.Common.Infra.Helper.Serializer;
using FoodDelivery.Common.Infra.Messaging.Services;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.ProductAggregate;
using FoodDelivery.Microservices.OrderMicroservice.Application.CQRS.Commands;
using FoodDelivery.Microservices.OrderMicroservice.Application.Services;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Contexts;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Repositories;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.OrderWorker
{
    public class Functions
    {
        private static IWorkerApplicationService workerApplicationService = new WorkerApplicationService(new OrderCommandHandler(new OrderService(new ProductQueryService(new ProductMicroserviceQueryRepository(new SerializerService())), new OrderRepository(new OrderContext()))), new AzureServiceBusQueue());
        private static ISerializerService serializerService = new SerializerService();

        //public Functions(IWorkerApplicationService workerApplicationService, ISerializerService serializerService)
        //{
        //    this.workerApplicationService = workerApplicationService;
        //    this.serializerService = serializerService;
        //}

        //[Singleton("ProductUpdateLock", SingletonScope.Host)]
        public static async Task ProcessOrderCommandFunction([ServiceBusTrigger(ProcessOrderCommand.CommandQueueName)] string message, ILogger logger)
        {
            logger.LogInformation(message);
            var command = serializerService.Deserialize<ProcessOrderCommand>(message);
            await workerApplicationService.ProcessOrderAsync(command);
        }
    }
}


