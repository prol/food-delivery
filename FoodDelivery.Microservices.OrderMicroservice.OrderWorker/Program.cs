﻿using FoodDelivery.Common.Application.Interfaces.CQRS.Messaging;
using FoodDelivery.Common.Domain.Interfaces.Services;
using FoodDelivery.Common.Infra.Helper.Serializer;
using FoodDelivery.Common.Infra.Messaging.Services;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.ProductAggregate;
using FoodDelivery.Microservices.OrderMicroservice.Application.Services;
using FoodDelivery.Microservices.OrderMicroservice.OrderWorker.Properties;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Contexts;
using FoodDelivery.OrderMicroservice.Infra.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Threading.Tasks;

namespace FoodDelivery.Microservices.OrderMicroservice.OrderWorker
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new HostBuilder();
            builder.ConfigureServices(services =>
            {
                services.AddSingleton<Functions, Functions>();
                services.AddSingleton<IMediatorHandler, AzureServiceBusQueue>();
                services.AddSingleton<IProductQueryRepository, ProductMicroserviceQueryRepository>();
                services.AddSingleton<DbContext, OrderContext>();
                services.AddSingleton<IOrderRepository, OrderRepository>();
                services.AddSingleton<IOrderService, OrderService>();
                services.AddSingleton<IProductQueryService, ProductQueryService>();
                services.AddSingleton<ISerializerService, SerializerService>();
                services.AddSingleton<IWorkerApplicationService, WorkerApplicationService>();
            }).ConfigureWebJobs(b =>
            {
                b.AddAzureStorageCoreServices();
                b.AddServiceBus(sbOptions =>
                {
                    sbOptions.ConnectionString = Resources.ServiceBusConnectionString;
                    sbOptions.MessageHandlerOptions.AutoComplete = true;
                    sbOptions.MessageHandlerOptions.MaxConcurrentCalls = 16;
                });
            });
            var host = builder.Build();
            using (host)
            {
                await host.RunAsync();
            }
        }
    }
}