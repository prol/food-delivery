﻿using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.OrderMicroservice.Infra.DataAccess.Contexts
{
    public class OrderContext : DbContext
    {
        private readonly string dbConnectionString;
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }

        public OrderContext()
        {
            this.dbConnectionString = "Server=tcp:fooddeliveryorderdbserver.database.windows.net,1433;Initial Catalog=FoodDelivery.Order.Db;Persist Security Info=False;User ID=administrador;Password=DEP@1317#alpha;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            Database.EnsureCreated();
        }

        public OrderContext(string dbConnectionString)
        {
            this.dbConnectionString = dbConnectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Server=tcp:fooddeliveryorderdbserver.database.windows.net,1433;Initial Catalog=FoodDelivery.Order.Db;Persist Security Info=False;User ID=administrador;Password=DEP@1317#alpha;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Order>()
            //    .HasMany(c => c.OrderItems)
            //    .WithOne(e => e.Order);
        }
    }
}