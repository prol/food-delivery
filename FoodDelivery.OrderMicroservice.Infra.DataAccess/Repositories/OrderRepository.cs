﻿using FoodDelivery.Common.Infra.DataAccess.Repositories;
using FoodDelivery.Microservice.OrderMicroservice.Domain.AggregatesModel.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.OrderMicroservice.Infra.DataAccess.Repositories
{
    public class OrderRepository : EntityFrameworkRepositoryBase<Guid, Order>, IOrderRepository
    {
        public OrderRepository(DbContext dbContext)
            : base(dbContext) { 
    }
}
}
