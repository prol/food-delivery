﻿using FoodDelivery.Common.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.ProductMicroservice.Domain.AggregatesModel.ProductAggregate
{
    public interface IProductRepository: IRepository<Guid, Product>
    {
    }
}
