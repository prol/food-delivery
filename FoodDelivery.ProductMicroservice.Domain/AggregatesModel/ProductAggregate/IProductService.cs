﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace FoodDelivery.ProductMicroservice.Domain.AggregatesModel.ProductAggregate
{
    public interface IProductService
    {
        //User Cases related to product
        Task <bool> AddProductAsync(Product product);
        IEnumerable<Product> GetAllProducts();
        Task<Product> GetProductAsync(Guid productId);
        Task <IEnumerable<Product>> GetAllProductsAsync();
        Task <bool> UpdateProductAsync(Product product);
        Task <bool> DeleteProductAsync(Guid productId);
        
    }
}
