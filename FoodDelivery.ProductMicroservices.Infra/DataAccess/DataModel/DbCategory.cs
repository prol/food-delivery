﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace FoodDelivery.ProductMicroservices.Infra.DataAccess.DataModel
{
    public class DbCategory
    {
        [Key]
        public string Name { get; set; }
    }
}
