﻿using FoodDelivery.ProductMicroservice.Domain.AggregatesModel.ProductAggregate;
using FoodDelivery.ProductMicroservices.Infra.DataAccess.DataModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.ProductMicroservices.Infra.DataAccess
{
    public class ProductContext: DbContext
    {
        private readonly string dbConnectionString;
        public DbSet<Product> Products { get; set; }
        //public DbSet<DbCategory> Categories { get; set; }

        public ProductContext()
        {
            this.dbConnectionString = "Server=tcp:fooddelivery-product-db-server.database.windows.net,1433;Initial Catalog=fooddelivery.product.db;Persist Security Info=False;User ID=administrador;Password=DEP@1317#alpha;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            Database.EnsureCreated();
        }


        public ProductContext(string dbConnectionString)
        {
            this.dbConnectionString = dbConnectionString;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(dbConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

           // modelBuilder
           //      .Entity<Product>()
           //     .Property(product => product.category)
           //     .HasConversion(
           //     category => category.ToString(),
           //     category => Category.Parse(category))
           //     .HasColumnName("Category");

          //  modelBuilder
          //      .Entity<DbCategory>()
          //      .Property(dbCategory => dbCategory.Name)
          //      .HasConversion(
          //      category => category.ToString(),
          //      category => new Category(category))
          //      .HasColumnName("Category");

        }
    }
}
