﻿
using FoodDelivery.Common.Infra.DataAccess.Repositories;
using FoodDelivery.ProductMicroservice.Domain.AggregatesModel.ProductAggregate;
using FoodDelivery.ProductMicroservices.Infra.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace FoodDelivery.ProductMicroservices.Infra.Repositories.Products
{
    public class AzureSqlServerProductsRepository : EntityFrameworkRepositoryBase<Guid, Product>, IProductRepository
    {
        public AzureSqlServerProductsRepository()
            :base(new ProductContext("Server=tcp:fooddelivery-product-db-server.database.windows.net,1433;Initial Catalog=fooddelivery.product.db;Persist Security Info=False;User ID=administrador;Password=DEP@1317#alpha;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;"))
        {
           
        }
    }
}
