﻿using Deliveryapp.Infra.DataAccess.Repositories.Products;
using fooddelivery.Views;
using fooddelivery.Views.Login;
using FoodDelivery.App.Aplication;
using FoodDelivery.App.Domain.Services;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace fooddelivery
{
    public partial class App : Application
    {
        public static IAppService AppService { get; set; }
        public static ProductLocalService Service { get; set; }

        
        public App()
        {
            InitializeComponent();
            AppService = new CustomerMobileAppService();
            Service = new ProductLocalService(new SQLiteProductsRepository(Device.RuntimePlatform));
            MainPage = new NavigationPage(new SignInPage());

        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }


    }
        
    }

