﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace fooddelivery.Views.Login
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SucessfullSignUp : ContentPage
    {
        public SucessfullSignUp()
        {
            InitializeComponent();
        }

        private void ButtonContinue_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new SignInPage());

        }
    }
}